package org.acme.graph.model;

import java.util.ArrayList;
import java.util.List;

public class Path {

    private List<Edge> edges;

    public Path(List<Edge> edges){
        this.edges = edges;
    }

    public double getlength(){
        double sumEdges = 0;
        for (int i=0; i<this.edges.size(); i++){
            sumEdges += this.edges.get(i).getCost();
        }
        return sumEdges;
    }

    public List<Edge> getEdges(){
        return this.edges;
    }

}
