package org.acme.graph.routing;

import org.acme.graph.model.Edge;
import org.acme.graph.model.Graph;
import org.acme.graph.model.Path;
import org.acme.graph.model.Vertex;

import java.util.*;

public class PathTree {

    private Graph graph;
    private Map<Vertex, PathNode> nodes;

    /**
     * Prépare le graphe pour le calcul du plus court chemin
     *
     *
     */
    public PathTree(Graph graph, Vertex origin){
        this.graph = graph;
        this.nodes = new HashMap<Vertex, PathNode>();
        for (Vertex vertex : graph.getVertices()) {
            PathNode pathnode = new PathNode();
            pathnode.setCost(origin == vertex ? 0.0 : Double.POSITIVE_INFINITY);
            pathnode.setReachingEdge(null);
            pathnode.setVisited(false);
            this.nodes.put(vertex, pathnode);
        }
    }

    /**
     * Construit le chemin en remontant les relations incoming edge
     *
     * @param destination
     * @return
     */
    public Path getPath(Vertex destination) {
        List<Edge> result = new ArrayList<>();

        Edge current = getNode(destination).getReachingEdge();
        do {
            result.add(current);
            current = getNode(current.getSource()).getReachingEdge();
        } while (current != null);

        Collections.reverse(result);
        return new Path(result);
    }

    public PathNode getNode (Vertex vertex){
        return this.nodes.get(vertex);
    }


}
